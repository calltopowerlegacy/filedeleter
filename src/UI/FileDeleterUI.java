/**
 * FileDeleter Copyright 2013-2014 Denis Meyer
 * https://sites.google.com/site/calltopowersoftware/software
 */
package UI;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

public class FileDeleterUI extends javax.swing.JFrame {

    private String currentDirectory = "";
    private int nrOfDeletedFolders = 0;
    private int nrOfDeletedFiles = 0;
    private int nrOfNotDeletedFolders = 0;
    private int nrOfNotDeletedFiles = 0;

    public FileDeleterUI() {
        initComponents();

        if (!System.getProperty("os.name").startsWith("Mac")) {
            this.setTitle(this.getTitle() + ", Copyright 2013-2014 Denis Meyer, Version 1.0.0");
        }

        this.setLocationRelativeTo(null);

        button_select.requestFocus();
    }

    private void enableAllComponents(boolean enable) {
        button_check.setEnabled(enable);
        button_select.setEnabled(enable);
        button_start.setEnabled(enable);
        spinner_filesize.setEnabled(enable);
    }

    private void delete(File directory, float maxFileSize) {
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                if (file.isFile()) {
                    float fileSizeInBytes = file.length();
                    float fileSizeInKB = fileSizeInBytes / 1024f;
                    float fileSizeInMB = fileSizeInKB / 1024f;
                    if (fileSizeInMB < maxFileSize) {
                        if (file.delete()) {
                            ++nrOfDeletedFiles;
                            textarea_output.append(
                                    "Deleting file \""
                                    + file.getAbsolutePath()
                                    + "\" ("
                                    + fileSizeInMB
                                    + " MB)"
                                    + "\n");
                        } else {
                            ++nrOfNotDeletedFiles;
                            textarea_output.append(
                                    "Failed deleting file \""
                                    + file.getAbsolutePath()
                                    + "\" ("
                                    + fileSizeInMB
                                    + " MB)"
                                    + "\n");
                        }
                    }
                } else if (file.isDirectory()) {
                    delete(file, maxFileSize);
                }
            }
            if (directory.listFiles().length <= 0) {
                if (directory.delete()) {
                    ++nrOfDeletedFolders;
                    textarea_output.append(
                            "Deleting folder \""
                            + directory.getAbsolutePath()
                            + "\"\n");
                } else {
                    ++nrOfNotDeletedFolders;
                    textarea_output.append(
                            "Failed deleting folder \""
                            + directory.getAbsolutePath()
                            + "\"\n");
                }
            }
        }
    }

    private void delete(final float maxFileSize) {
        final Component component = this;
        new Thread() {
            @Override
            public void run() {
                try {
                    enableAllComponents(false);
                    File directory = new File(currentDirectory);
                    try {
                        delete(directory, maxFileSize);
                        String deleted = ((nrOfDeletedFolders > 0) || (nrOfDeletedFiles > 0)) ? ("Successfully deleted " + nrOfDeletedFolders + " folders and " + nrOfDeletedFiles + " files.") : "Deletion complete.";
                        String notDeleted = ((nrOfNotDeletedFolders > 0) || (nrOfNotDeletedFiles > 0)) ? ("\nCould not delete " + nrOfNotDeletedFolders + " folders and " + nrOfNotDeletedFiles + " files.") : "";
                        JOptionPane.showMessageDialog(
                                component,
                                deleted + notDeleted,
                                "Deletion complete",
                                JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(
                                component,
                                "Something went wrong.",
                                "Deletion incomplete",
                                JOptionPane.WARNING_MESSAGE);
                    }
                } catch (Exception e) {
                } finally {
                    enableAllComponents(true);
                }
            }
        }.start();
    }

    private void check(File directory, float maxFileSize) {
        if (directory.isDirectory()) {
            for (File file : directory.listFiles()) {
                if (file.isFile()) {
                    float fileSizeInBytes = file.length();
                    float fileSizeInKB = fileSizeInBytes / 1024f;
                    float fileSizeInMB = fileSizeInKB / 1024f;
                    if (fileSizeInMB < maxFileSize) {
                        ++nrOfDeletedFiles;
                    }
                } else if (file.isDirectory()) {
                    ++nrOfDeletedFolders;
                    check(file, maxFileSize);
                }
            }
        }
    }

    private void check(final float maxFileSize) {
        final Component component = this;
        new Thread() {
            @Override
            public void run() {
                try {
                    enableAllComponents(false);
                    File directory = new File(currentDirectory);
                    try {
                        check(directory, maxFileSize);
                        JOptionPane.showMessageDialog(
                                component,
                                "Destination folder: \"" + currentDirectory + "\"\n"
                                + "Max. file size < " + maxFileSize + " MB\n\n"
                                + "Number of folders to be deleted: " + (nrOfDeletedFolders + 1) + "\n"
                                + "Number of files to be deleted: " + nrOfDeletedFiles + "\n",
                                "Deletion information",
                                JOptionPane.INFORMATION_MESSAGE);
                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(
                                component,
                                "Something went wrong.",
                                "Deletion incomplete",
                                JOptionPane.WARNING_MESSAGE);
                    }
                } catch (Exception e) {
                } finally {
                    enableAllComponents(true);
                }
            }
        }.start();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        panel_main = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        label_directory = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        button_select = new javax.swing.JButton();
        textfield_directory = new javax.swing.JTextField();
        label_size = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        spinner_filesize = new javax.swing.JSpinner();
        jPanel6 = new javax.swing.JPanel();
        button_check = new javax.swing.JButton();
        button_start = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        textarea_output = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("FileDeleter");
        setMaximumSize(null);
        setMinimumSize(new java.awt.Dimension(644, 256));
        setPreferredSize(new java.awt.Dimension(644, 256));
        getContentPane().setLayout(new java.awt.GridBagLayout());

        panel_main.setBorder(javax.swing.BorderFactory.createEmptyBorder(5, 5, 5, 5));
        panel_main.setLayout(new java.awt.GridBagLayout());

        jPanel2.setLayout(new java.awt.GridBagLayout());

        label_directory.setText("Directory:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel2.add(label_directory, gridBagConstraints);

        jPanel9.setLayout(new java.awt.GridBagLayout());

        button_select.setText("Select");
        button_select.setNextFocusableComponent(spinner_filesize);
        button_select.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_selectActionPerformed(evt);
            }
        });
        button_select.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                button_selectKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel9.add(button_select, gridBagConstraints);

        textfield_directory.setEditable(false);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel9.add(textfield_directory, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(jPanel9, gridBagConstraints);

        label_size.setText("Max. file size:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel2.add(label_size, gridBagConstraints);

        jPanel10.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("MB");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        jPanel10.add(jLabel1, gridBagConstraints);

        spinner_filesize.setModel(new javax.swing.SpinnerNumberModel(Float.valueOf(0.1f), Float.valueOf(0.0f), null, Float.valueOf(0.1f)));
        spinner_filesize.setNextFocusableComponent(button_check);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel10.add(spinner_filesize, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel2.add(jPanel10, gridBagConstraints);

        jPanel6.setLayout(new java.awt.GridBagLayout());

        button_check.setText("Check deletion");
        button_check.setNextFocusableComponent(button_start);
        button_check.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_checkActionPerformed(evt);
            }
        });
        button_check.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                button_checkKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(button_check, gridBagConstraints);

        button_start.setText("Start deletion");
        button_start.setNextFocusableComponent(button_select);
        button_start.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button_startActionPerformed(evt);
            }
        });
        button_start.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                button_startKeyReleased(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        jPanel6.add(button_start, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        jPanel2.add(jPanel6, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        panel_main.add(jPanel2, gridBagConstraints);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        textarea_output.setEditable(false);
        textarea_output.setColumns(20);
        textarea_output.setRows(5);
        jScrollPane2.setViewportView(textarea_output);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jScrollPane2, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        panel_main.add(jPanel1, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        getContentPane().add(panel_main, gridBagConstraints);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void button_selectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_selectActionPerformed
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle("Choose a directory");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            currentDirectory = (String) chooser.getSelectedFile().getAbsolutePath();
            textfield_directory.setText(currentDirectory);
            button_check.requestFocus();
        } else {
            button_select.requestFocus();
        }
    }//GEN-LAST:event_button_selectActionPerformed

    private void button_startActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_startActionPerformed
        if (!currentDirectory.isEmpty()) {
            File folder = new File(currentDirectory);
            if (folder.isDirectory() && folder.canRead() && folder.canWrite()) {
                float maxFileSize = (float) spinner_filesize.getValue();
                if (JOptionPane.showConfirmDialog(
                        this,
                        "Do you really want to recursively delete all files with the following specifications?\n\n"
                        + "Destination folder: \"" + currentDirectory + "\"\n"
                        + "Max. file size < " + maxFileSize + " MB",
                        "Confirm deletion of files",
                        JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                    nrOfDeletedFolders = 0;
                    nrOfDeletedFiles = 0;
                    nrOfNotDeletedFolders = 0;
                    nrOfNotDeletedFiles = 0;
                    textarea_output.setText("");
                    textarea_output.append("Destination folder: \"" + currentDirectory + "\"\n");
                    textarea_output.append("Max. file size < " + maxFileSize + "\n");
                    delete(maxFileSize);
                    button_check.requestFocus();
                }
            } else {
                JOptionPane.showMessageDialog(
                        this,
                        "You don't have enough permissions to read from or write to the selected directory.",
                        "No permissions",
                        JOptionPane.OK_OPTION);
                button_select.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(
                    this,
                    "You did not select a directory, yet.",
                    "No directory selected",
                    JOptionPane.OK_OPTION);
        }
    }//GEN-LAST:event_button_startActionPerformed

    private void button_checkActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button_checkActionPerformed
        if (!currentDirectory.isEmpty()) {
            File folder = new File(currentDirectory);
            if (folder.isDirectory() && folder.canRead() && folder.canWrite()) {
                float maxFileSize = (float) spinner_filesize.getValue();
                nrOfDeletedFolders = 0;
                nrOfDeletedFiles = 0;
                nrOfNotDeletedFolders = 0;
                nrOfNotDeletedFiles = 0;
                textarea_output.setText("");
                check(maxFileSize);
                button_start.requestFocus();
            } else {
                JOptionPane.showMessageDialog(
                        this,
                        "You don't have enough permissions to read from or write to the selected directory.",
                        "No permissions",
                        JOptionPane.OK_OPTION);
                button_select.requestFocus();
            }
        } else {
            JOptionPane.showMessageDialog(
                    this,
                    "You did not select a directory, yet.",
                    "No directory selected",
                    JOptionPane.OK_OPTION);
            button_select.requestFocus();
        }
    }//GEN-LAST:event_button_checkActionPerformed

    private void button_selectKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_button_selectKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            button_selectActionPerformed(null);
        }
    }//GEN-LAST:event_button_selectKeyReleased

    private void button_checkKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_button_checkKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            button_checkActionPerformed(null);
        }
    }//GEN-LAST:event_button_checkKeyReleased

    private void button_startKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_button_startKeyReleased
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            button_startActionPerformed(null);
        }
    }//GEN-LAST:event_button_startKeyReleased

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FileDeleterUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FileDeleterUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FileDeleterUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FileDeleterUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FileDeleterUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton button_check;
    private javax.swing.JButton button_select;
    private javax.swing.JButton button_start;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel label_directory;
    private javax.swing.JLabel label_size;
    private javax.swing.JPanel panel_main;
    private javax.swing.JSpinner spinner_filesize;
    private javax.swing.JTextArea textarea_output;
    private javax.swing.JTextField textfield_directory;
    // End of variables declaration//GEN-END:variables
}
