/**
 * FileDeleter Copyright 2013-2014 Denis Meyer
 * https://sites.google.com/site/calltopowersoftware/software
 */
package UI;

import Util.Constants;
import java.awt.BorderLayout;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;

@SuppressWarnings("serial")
public class SplashScreen extends JWindow {

    private JLabel imageLabel = null;
    private ImageIcon imageIcon = null;
    private BorderLayout borderLayout = null;

    public SplashScreen() {
        URL splashIconURL = getClass().getClassLoader().getResource("img/splashscreen.png");
        if (splashIconURL != null) {
            this.imageIcon = new ImageIcon(splashIconURL);

            borderLayout = new BorderLayout();
            imageLabel = new JLabel();

            initLayout();
        }
    }

    private void initLayout() {
        // Set the (upper) Image
        if (imageIcon != null) {
            imageLabel.setIcon(imageIcon);
        }

        setLayout(borderLayout);
        add(imageLabel, BorderLayout.CENTER);

        pack();

        this.setLocationRelativeTo(null);
    }

    public void setVisible() {
        setVisible(true);
        for (int i = 0; i <= 100; ++i) {
            try {
                Thread.sleep(Constants.MS_SPLASHSCREEN);
            } catch (InterruptedException ex) {
            }
        }
        dispose();
    }
}
