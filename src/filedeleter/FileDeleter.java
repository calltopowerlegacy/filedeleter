/**
 * FileDeleter Copyright 2013-2014 Denis Meyer
 * https://sites.google.com/site/calltopowersoftware/software
 */
package filedeleter;

import UI.FileDeleterUI;
import UI.SplashScreen;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class FileDeleter {

    private static void initPlatformSpecifications() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Comment in when bundling for Mac OS X!
        if (System.getProperty("os.name").startsWith("Mac")) {
            System.setProperty("apple.laf.useScreenMenuBar", "true");
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "FileDeleter");
            System.setProperty("com.apple.mrj.application.live-resize", "true");
            System.setProperty("com.apple.mrj.application.growbox.intrudes", "false");
            System.setProperty("com.apple.macos.smallTabs", "true");
        }
        FileDeleterUI fdui = new FileDeleterUI();
        if (System.getProperty("os.name").startsWith("Mac")) {
            MacOSHandler macOSHandler = new MacOSHandler(fdui);
        }
        initPlatformSpecifications();
        new SplashScreen().setVisible();
        fdui.setVisible(true);
    }
}
