/**
 * FileDeleter Copyright 2013-2014 Denis Meyer
 * https://sites.google.com/site/calltopowersoftware/software
 */
package filedeleter;

// Comment in when bundling for Mac OS X!
import UI.About;
import UI.FileDeleterUI;
import com.apple.eawt.Application;
import com.apple.eawt.ApplicationEvent;
import com.apple.eawt.ApplicationListener;
// import javax.swing.JOptionPane;

/**
 * Handles Mac OS-specific Settings
 *
 * @author Denis Meyer (CallToPower)
 */
// Comment in when bundling for Mac OS X!
@SuppressWarnings("deprecation")
public class MacOSHandler extends Application {

    /**
     * Mac OS X Settings Handler
     *
     * @param s FileDeleterUI
     */
// Comment in when bundling for Mac OS X!
    public MacOSHandler(final FileDeleterUI s) {
        addApplicationListener(new ApplicationListener() {
            @Override
            public void handleAbout(ApplicationEvent event) {
                event.setHandled(true);
                About about = new About(s);
                about.setVisible(true);
            }

            @Override
            public void handleOpenApplication(ApplicationEvent event) {
                // Do nothing
            }

            @Override
            public void handleOpenFile(ApplicationEvent event) {
                // Do nothing
            }

            @Override
            public void handlePreferences(ApplicationEvent event) {
                // Do nothing
            }

            @Override
            public void handlePrintFile(ApplicationEvent event) {
                // Do nothing
            }

            @Override
            public void handleQuit(ApplicationEvent event) {
                event.setHandled(true);
                System.exit(0);
            }

            @Override
            public void handleReOpenApplication(ApplicationEvent event) {
                // Do nothing
            }
        });
        this.setEnabledPreferencesMenu(false);
    }
}
